import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

/**
  * proj
  * @author Iorga Antonio, Walid Jawahir
  * Ce travail a été partagé en deux
  * on était sur le point de finir le projet mais ça nous manque quelques methodes pour le faire marcher: data,put,get et la classe MyMessage
  */
public class Proj {
 
    /**
     * La classe MyObject c'est une metaclass qu'on va utiliser pour créer nos propres objets en java
     */
    public class MyObject {
        
        /**
         * La methode data
         * On sait qu'elle doit avoir cette forme
         */
        private final Map<String, Object> data = new TreeMap<>(); 
        
        /**
         * La methode pour trouver
         * @param cls
         * @param nom
         * @param f
         * @return
         */
        private static MyMessage findTheMethod (MyObject cls,String nom, Function<MyObject,Map<String,MyMessage>> f) {

            var classActuelle = cls;

            while (classActuelle != null) {
                MyMessage method = f.apply(classActuelle).get(nom);
                if (method != null) {
                    return method;
                }

                classActuelle = (MyObject) classActuelle.data().get("superClass");
            }

            return cls.error(String.format("method %s could not be found"),nom);
        }

        /**
         * La methode pour trouver la classe de declaration
         * @param fils
         * @param nom
         * @return
         */
        private MyObject findDeclaringClass (MyObject fils, String nom) {
            var classActuelle = fils.classe();

            while (classActuelle != null) {

                Map <String,MyMessage> messages = Proj.messages(classActuelle);
                MyMessage method = messages.get(nom);
                if(method != null) {
                    return classActuelle;
                }
                classActuelle = (MyObject) classActuelle.data().get("superClass");
            }
            return null;
        }

        /**
         * on n'a pas pu fiare cette methode
         * @return
         */
        private Object classe() {
            return null;
        }

        /**
         * à l'aide de cette methode on va communiquer entre les classes
         * @param <T>
         * @param nom
         * @param arguments
         * @return
         */
        public <T> T message(String nom, Object... arguments) {
            Map<String, MyMessage> lcMessage = (Map<String,MyMessage>) data().get("messages");

            if (locMessage != null) {
                
                MyMessage locMethod = lcMessage.get(nom);
                if (lcMethod != null) {
                    return (T) locMethod.apply(this, arguments);
                }
            }

            return (T) findTheMethod(classe(), nom, (c) -> (Map<String, MyMessage>) c.data().get("messages")).apply(this.arguments);
        }

        /**
         * à l'aide de cette methode on va communiquer entre les classes
         * la difference par rapport à la methode message c'est qu'on cherche la classe de declaration pas la classe locale
         * @param <T>
         * @param nom
         * @param arguments
         * @return
         */
        public <T> T superMessage(String nom, Object... arguments) {

            MyObject var = findDeclaringClass(this, nom);

            if (var != null){
                return (T) findTheMethod ((MyObject) var.data().get("superClass"), nom, (c) -> (Map<String,MyMessage>) c.data().get("messages")).apply(this.arguments);
            }
            return error(String.format("method %s is unknown"), nom);
        }

        /**
         * 
         * @param <T>
         * @param errorName
         * @return une erreur si la classe n'est pas bonne
         * 
         */
        public <T> T error(String errorName) {
            throw new IllegalArgumentException(errorName);
        }

        /**
         * Method toString pour retourner une chaine de caracteres avec le nom de la classe
         */
        public String toString() {

            Object nomClass = data().get("className");
            if (nomClass == null) {
                return  data().get("class");
            }
            return "--" + nomClass + "--";
        }
    }

    public static Map<String, MyMessage> messages(Object classActuelle) {
        return null;
    }

    /**
     * On a essayé de créer nos propres objets
     * La classe rtClasse c'est la superClass qui va être la classe mere de mtClass
     * @param args
     */
    public static void main(String[] args) {
        mtClass = new MyObject();
        rtClass = new MyObject(mtClass);

        mtClass.data().put("class",mtClass);
        mtClass.data().put("className","Class");
        mtClass.data().put("superClass",rtClass);
        
        rtClass.data().put("className","Object");
        rtClass.data().put("superClass",null);
    }

}